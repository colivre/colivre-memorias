---
layout: post
title:  "O embrião do que viria a ser a COLIVRE"
date:   '2023-04-09 08:00:00 -0300'
author: Antonio Terceiro
---

A ideia do que viria a ser a COLIVRE nasceu enquanto eu e outros amigos
estávamos no curso de Ciência da Computação na UFBA. Tínhamos um grupo muito
forte de software livre, e percebíamos um atraso no mercado de trabalho local
com relação à adoção de software livre. O grupo do software livre tinha uma
grande interseção com o grupo do movimento estudantil, organizado no Diretório
Acadêmico (DACOMP). Além da representação estudantil junto ao colegiado do
curso e ao departamento de Ciência da Computação, realizávamos muitas
atividades: trote solidário, minicurso de GNU/Linux para os calouros, eventos
de software livre, seminários acadêmicos.

Estávamos em 2002, e alguns de nós começavam a vislumbrar a formatura, e a
perspectiva de ter que trabalhar com software proprietário nos fez começar a
discutir a possibilidade de criar as nossas próprias oportunidades de trabalho
onde isso não fosse necessário.

Nós não sabíamos exatamente o que fazer, mas definitivamente queríamos fundar
uma organização que trabalhasse com software livre. Esse projeto recebeu
inicialmente o codinome de GNUPIA, e inicialmente pensamos em algo como uma
ONG, mas ainda em 2002 já sabíamos que na verdade o que a gente precisava era
uma cooperativa. Boa parte dessas discussões iniciais foram documentadas no
[wiki do DCC][gnupia].

[gnupia]: http://wiki.dcc.ufba.br/GNUPIA/WebHome

![Logo da GNUPIA](/img/gnupia.gif)
![Logo da GNUPIA pequeno com sombra projetada](/img/gnupia2.gif)

O grupo de definia da seguinte forma:

> Grupo de interesse na difusão do software livre e da filosofia de liberdade
> de informação. União que promove a Propriedade Intelectual Livre para
> contribuir com a educação e desenvolvimento da comunidade.

No wiki existe o registro de pelo menos 7 reuniões do grupo que estava
trabalhando nisso. Havia um sistema para que cada futuro cooperado colocasse
suas experiências, e os serviços que se propunha a realizar, mas a [lista de
serviços prestados coletivamente pela cooperativa][servicos] nunca chegou a ser
compilada.

[servicos]: http://wiki.dcc.ufba.br/GNUPIA/ServicosPropostos

Chegamos até a ter um [esboço de estatuto][estatuto], mas mas a vida aconteceu,
e 2002 não foi o ano em que fundamos uma cooperativa.

[estatuto]: http://wiki.dcc.ufba.br/GNUPIA/Estatuto
