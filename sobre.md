---
layout: page
title: Sobre
permalink: /sobre/
---

Este site é dedicado à COLIVRE. Aqui vamos, dentro do possível, coletar textos
e relatos sobre a história da cooperativa, e talvez um dia esse material se
torne um livro.

![Logo da COLIVRE](/img/colivre-logo.svg)

A última versão do site da COLIVRE está disponível [aqui][site].

[site]: https://siteantigo.colivre.coop.br/
